import http from 'http';
import WebSocket from 'ws';
import ChatServer from '../src/chat-server';

let webServer: any;
beforeAll((done) => {
  webServer = http.createServer();
  new ChatServer(webServer, '/api/v1');
  // Use separate port for testing
  webServer.listen(3001, () => done());
});

afterAll((done) => {
  if (!webServer) return done.fail(new Error());
  webServer.close(() => done());
});

describe('ChatServer tests', () => {
  test('Connection opens successfully', (done) => {
    const connection = new WebSocket('ws://localhost:3001/api/v1/chat');

    connection.on('open', () => {
      connection.close();
      done();
    });

    connection.on('error', (error) => {
      done.fail(error);
    });
  });

  test('ChatServer replies correctly', (done) => {
    const connection = new WebSocket('ws://localhost:3001/api/v1/chat');

    connection.on('open', () => connection.send('test'));

    connection.on('message', (data) => {
      expect(data.toString()).toEqual('test');
      connection.close();
      done();
    });

    connection.on('error', (error) => {
      done.fail(error);
    });
  });
});
