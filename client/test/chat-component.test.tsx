import * as React from 'react';
import { Chat } from '../src/chat-component';
import { shallow } from 'enzyme';

jest.mock('../src/chat-service', () => {
  class Subscription {
    onopen = () => {};
  }

  class ChatService {
    constructor() {}

    subscribe() {
      const subscription = new Subscription();

      // Call subscription.onopen after subscription is returned
      setTimeout(() => subscription.onopen());

      return subscription;
    }
  }
  return new ChatService();
});

describe('Chat component tests', () => {
  test('draws correctly when connected', (done) => {
    // @ts-ignore: do not type check next line.
    const wrapper = shallow(<Chat />);

    expect(wrapper.containsMatchingElement(<div>Not connected</div>)).toEqual(true);

    setTimeout(() => {
      expect(wrapper.containsMatchingElement(<div>Connected</div>)).toEqual(true);
      done();
    });
  });
});
