import * as React from 'react';
import { Component } from 'react-simplified';
import chatService, { Subscription } from './chat-service';
import { Alert, Card, Column, Form, Row } from './widgets';
import { KeyboardEvent } from 'react';

export class Chat extends Component {
  subscription: Subscription | null = null;
  connected = false;
  users: string[] = [];
  messages: string[] = [];
  message = '';
  user = '';

  render() {
    return (
      <>
        <Card title={this.connected ? 'Chat(Connected)' : 'Chat(Not connected)'}>
          <Card title="Connected users">
            {this.users.map((user, index) => {
              return <div key={index}>{user}</div>;
            })}
          </Card>
          <Card title="Messages">
            {this.messages.map((message, index) => {
              return <div key={index}>{message}</div>;
            })}
          </Card>
          <Card title="New message">
            <Row>
              <Column width={2}>
                <Form.Input
                  type="text"
                  placeholder="Enter username"
                  value={this.user}
                  disabled={this.subscription}
                  onChange={(event) => {
                    this.user = event.currentTarget.value;
                  }}
                  onKeyUp={(event: KeyboardEvent<HTMLInputElement>) => {
                    if (event.key == 'Enter') { 
                      if (!this.subscription) {
                        this.subscription = chatService.subscribe();
                        this.subscription.onopen = () => {
                          this.connected = true;
                          chatService.send({ addUser: this.user });

                          // Remove user when web page is closed
                          window.addEventListener('beforeunload', () =>
                            chatService.send({ removeUser: this.user })
                          );
                        };
                        // Called on incoming message
                        this.subscription.onmessage = (message) => {
                          if ('text' in message) {
                            this.messages.push(message.text);
                          }
                          if ('users' in message) {
                            this.users = message.users;
                          }
                        };

                        // Called if connection is closed
                        this.subscription.onclose = (code, reason) => {
                          this.connected = false;
                          Alert.danger(
                            'Connection closed with code ' + code + ' and reason: ' + reason
                          );
                        };
                        // Called on connection error
                        this.subscription.onerror = (error) => {
                          this.connected = false;
                          Alert.danger('Connection error: ' + error.message);
                        };
                      }
                    }
                  }}
                />
              </Column>
              <Column>
                <Form.Input
                  placeholder="Message"
                  type="text"
                  value={this.message}
                  onChange={(event) => {
                    this.message = event.currentTarget.value;
                  }}
                  onKeyUp={(event: KeyboardEvent<HTMLInputElement>) => {
                    if (event.key == 'Enter') {
                      if (this.connected) {
                        chatService.send({ text: this.user + ': ' + this.message });
                        this.message = '';
                      } else Alert.danger('Not connected to server');
                    }
                  }}
                />
              </Column>
            </Row>
          </Card>
        </Card>
      </>
    );
  }

  // Unsubscribe from chatService when component is no longer in use
  beforeUnmount() {
    if (this.subscription) chatService.unsubscribe(this.subscription);
  }
}
